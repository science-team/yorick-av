yorick-av (0.0.5-1) unstable; urgency=low

  * New upstream release.
  * Bug fix: "FTBFS with FFmpeg 4.0", thanks to jcowgill@debian.org</a>;
    (Closes: #888359).
  * Check against Policy 4.1.4 (priority optional, https in copyright
    format).
  * Add autopkgtest suite.

 -- Thibaut Paumard <thibaut@debian.org>  Wed, 30 May 2018 03:11:11 +0200

yorick-av (0.0.4-2) unstable; urgency=low

  * Bug fix: "AVPacket timestamps need to be rescaled for most codecs"
    (Closes: #890880).
  * Bug fix: "Need to set VBV buffer size for MPEG1/2 files", (Closes:
    #892176).

 -- Thibaut Paumard <thibaut@debian.org>  Tue, 06 Mar 2018 14:01:45 +0100

yorick-av (0.0.4-1) unstable; urgency=low

  * New upstream release

 -- Thibaut Paumard <thibaut@debian.org>  Wed, 30 Nov 2016 11:57:04 +0100

yorick-av (0.0.3-4) unstable; urgency=low

  * Bug fix: "FTBFS with FFmpeg 2.9", thanks to Andreas Cadhalpun (Closes:
    #803876).

 -- Thibaut Paumard <thibaut@debian.org>  Fri, 08 Jan 2016 02:15:25 +0000

yorick-av (0.0.3-3) unstable; urgency=low

  * Disable tests on kfreebsd-i386. Alleviates #747976.

 -- Thibaut Paumard <thibaut@debian.org>  Tue, 13 May 2014 21:50:19 +0200

yorick-av (0.0.3-2) unstable; urgency=low

  * Correct minor mistake in package description.

 -- Thibaut Paumard <thibaut@debian.org>  Wed, 19 Feb 2014 16:36:46 +0100

yorick-av (0.0.3-1) unstable; urgency=low

  * New upstream release. Fixes bug: "FTBFS with libav10", thanks to
    Moritz Muehlenhoff (Closes: #739377).
  * Bump Standards-Version to 3.9.5 (no changes needed)

 -- Thibaut Paumard <thibaut@debian.org>  Wed, 19 Feb 2014 12:29:26 +0100

yorick-av (0.0.2-1) unstable; urgency=low

  * New upstream release
    + fixes bug: "FTBFS on armhf: ERROR (*main*)
      Segmentation violation interrupt (SIGSEGV)", thanks to Sebastian
      Ramacher (Closes: #722610).
    + includes patch libx264-segfaults
  * Update watch file to use github branch tarballs.

 -- Thibaut Paumard <thibaut@debian.org>  Wed, 16 Oct 2013 16:40:49 +0200

yorick-av (0.0.1-3) unstable; urgency=low

  * Bug fix: "FTBFS: ERROR (*main*) Floating point interrupt (SIGFPE)",
    thanks to Sebastian Ramacher (Closes: #721037). Simply don't use
    libx264 at build time, see patch libx264-segfaults.
  * Review against policy 3.9.4.
  * Fix VCS-* fields.

 -- Thibaut Paumard <thibaut@debian.org>  Tue, 03 Sep 2013 14:39:42 +0200

yorick-av (0.0.1-2) unstable; urgency=low

  * Move to maintenance to the debian-science team. Update debian/control
    to comply with their policy:
    + change Maintainer, put self in Uploaders;
    + DM-Upload-Allowed: yes;
    + fill-in Vcs-* fields;
    + Priority is extra.
  * Fix machine-readable copyright format.
  * Add lintian override about no fortified functions.
  * Fortify (don't rely on yorick to pass the right build flags)

 -- Thibaut Paumard <paumard@users.sourceforge.net>  Tue, 26 Jun 2012 14:04:00 +0200

yorick-av (0.0.1-1) unstable; urgency=low

  * Initial Release. Closes: #666498

 -- Thibaut Paumard <paumard@users.sourceforge.net>  Thu, 29 Mar 2012 15:47:42 +0200
